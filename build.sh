#!/bin/bash
build() {
  python setup.py build
  ./ul build-preferences
}

prepare() {
  pkgver=$(git describe --long --tags | sed -r 's/([^-]*-g)/r\1/;s/-/./g')
  sed -i "s/%VERSION%/${pkgver%.*.*}/g" setup.py
  find -iname "*.py" | xargs sed -i 's=\(^#! */usr/bin.*\)python3 *$=\1python='
}

package() {
  export PYTHONHASHSEED=0
  sudo cp build/share/applications/ulauncher.desktop /usr/share/applications
  sudo python setup.py install --root="/" --optimize=1 --skip-build
  sudo rm -rf /usr/share/ulauncher/preferences/{no*,src,bow*,gul*,pack*}
  sudo cp ulauncher.service /usr/lib/systemd/user/
}

prepare
build
package
